/**
 * Created by mconroy on 1/19/2016.
 */

var gulp = require('gulp');
var util = require('gulp-util');
var maps = require('gulp-sourcemaps');
var rename = require('gulp-rename');
var uncss = require('gulp-uncss');
var mincss = require('gulp-cssnano');
var minhtml = require('gulp-htmlmin');
var haml = require('gulp-ruby-haml');
var concat = require('gulp-concat');
var size = require('gulp-filesize');
var bs = require('browser-sync');
var reload = bs.reload;
var bump = require('gulp-bump');
var sequence = require('run-sequence').use(gulp);

/**
 * Main Build
 */

gulp.task('build',function(){
    sequence('haml','css','minify');
}); // end build

/**
 * Interpretors & Movers
 */

gulp.task('haml',function(){
    return gulp.src('./src/**/*.haml')
        .pipe(haml())
            .on('error',_errorHandler)
        .pipe(rename(function(path){
            path.extname = '.html';
        }))
        .pipe(gulp.dest('./dist'))
        .pipe(size());
}); // end haml

gulp.task('css',function(){
    return gulp.src('./src/**/*.css')
        .pipe(concat('site.css')) // create one site css file
        .pipe(uncss({
            html: ['./dist/**/*.html']  // remove all unused css styles
        }))
        .pipe(gulp.dest('./dist/css')); // save in the distribution css folder
}); // end css

gulp.task('homeCSS',function(){
    return gulp.src([
        './src/css/home/body.css',
        './src/css/home/text.css',
        './src/css/home/buttons.css',
        './src/css/home/background.css',
        './src/css/home/banner.css',
        './src/css/home/title-search.css',
        './src/css/home/nav.css',
        './src/css/home/page.css',
        './src/css/home/footer.css'
    ])
        .pipe(concat('home.css'))
        .pipe(uncss({
            html: ['./dist/home.html']
        }))
        .pipe(gulp.dest('./dist/css/home'))
        .pipe(size());
}); // end homeCSS

gulp.task('interiorCSS',function(){
    return gulp.src([
        './src/css/interior/nav.css',
        './src/css/interior/title-search.css',
        './src/css/interior/results-stats.css',
        './src/css/interior/page.css'
    ])
        .pipe(concat('interior.css'))
        .pipe(uncss({
            html: ['./dist/interior.html']
        }))
        .pipe(gulp.dest('./dist/css/interior'))
        .pipe(size());
}); // end interiorCSS

/**
 * Minification of HTML and CSS
 */

gulp.task('minify',['minHTML','minCSS']); // end minify

gulp.task('minHTML',function(){
    return gulp.src('./dist/**/*.html')
        .pipe(minhtml({
            collapseWhitespace: true
        }))
        .pipe(gulp.dest('./dist'));
}); // end minHTML

gulp.task('minCSS',function(){
    return gulp.src('./dist/**/*.css')
        .pipe(mincss())
        .pipe(gulp.dest('./dist'));
}); // end minCSS

/**
 * Run a Browswer and watch for script changes
 */

gulp.task('browserWatchHome',function(){
    var _files = [
        './dist/home.html',
        './dist/css/home/**/*.css'
    ];

    // start up browser
    bs.init(_files,{
        browser: ['google chrome'],
        ghostMode: true,
        server: {
            baseDir: './dist/',
            index: 'home.html'
        }
    });

    // watch for changes in files compile and reload
    gulp.watch('./src/home.haml',function(evt){
        util.log('File ' + evt.path + ' was ' + evt.type);
        // sequence('haml','minHTML',reload);
        sequence('haml',reload);
    });
    gulp.watch('./src/css/home/**/*.css',function(evt){
        util.log('File ' + evt.path + ' was ' + evt.type);
        // sequence('homeCSS','minCSS',reload);
        sequence('homeCSS',reload);
    });
}); // end browserWatchHome

gulp.task('browserWatchInterior',function(){
    var _files = [
        './dist/interior.html',
        './dist/css/interior/**/*.css'
    ];

    bs.init(_files,{
        browser: ['google chrome'],
        ghostMode: true,
        server: {
            baseDir: './dist/',
            index: 'interior.html'
        }
    });

    gulp.watch('./src/interior.haml',function(evt){
        util.log('File ' + evt.path + ' was ' + evt.type);
        sequence('haml',reload);
    }); // end watch
    gulp.watch('./src/css/interior/**/*.css',function(evt){
        util.log('File ' + evt.path + ' was ' + evt.type);
        sequence('interiorCSS',reload);
    }); // end watch
}); // end browserWatchInterior

/**
 * Version Changes
 */

gulp.task('version',function(){
    // gulp version --type [major|minor|patch]
    var type = util.env.type;
    if((type === 'major') || (type === 'minor') || (type === 'patch')){
        util.log('Bumping ' + type + ' version');
        return gulp.src(['./bower.json','./package.json'])
            .pipe(bump({type: type}))
            .pipe(gulp.dest('./'));
    }
    return;
});

/**
 * Error Handling
 */

var _errorHandler = function errorHandler(err){
    var _err = util.colors.red(err);
    util.log(_err);
    this.emit('end');
}; // end errorHandler